from django.apps import AppConfig


class AuthEnsConfig(AppConfig):
    name = "authens"
    default_auto_field = "django.db.models.AutoField"
