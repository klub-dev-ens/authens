��            )   �      �     �  J   �     �       �   '     �  
   �     �     �     	          5     G  $   T     y     �  G   �  	   �  {   �  !   s  e   �  ^   �  �   Z  	   
       !   %  C   G  	   �     �  D  �     �  I   �     C	     X	  �   i	     �	      
     
     
     (
     5
     L
     ^
     g
     �
     �
  B   �
     �
  e   �
     `  O   o  o   �  �   /     �     �     �  E        K     X                                                          
                                                          	                     Ancien compte CAS Ancien compte CAS %(cas_login)s (promo %(entrance_year)s) lié à %(user)s Ancien login clipper Anciens comptes CAS Aucun utilisateur n'existe avec ce clipper, cette promo et/ou ce mot de passe. Veuillez vérifier votre saisie. Attention, tous les champs sont sensibles à la casse ! Clipper Compte CAS Comptes CAS Connexion par mot de passe Connexion vieilleux L'équipe %(site_name)s Mode de connexion Mot de passe Mot de passe modifié avec succès ! Mot de passe oublié ? Nouveau mot de passe Pour information, votre nom d'utilisateur est le suivant : %(username)s Promotion Quelqu'un (probablement vous) a demandé la réinitialisation du mot de passe associé à cette addresse sur %(site_name)s. Réinitialisation du mot de passe S'il s'agit bien de vous, vous pouvez vous rendre à l'adresse suivante pour en choisir un nouveau :  Si un compte avec cet email existe, un email de réinitialisation vient de lui être envoyé ! Si votre fin de scolarité approche, créez un mot de passe pour votre compte pour bénéficier de la connexion vieilleux : <a href="%(url_reset)s">créer un mot de passe</a>. Vieilleux ancien login CAS année de création du compte CAS compte CAS %(cas_login)s (promo %(entrance_year)s) lié à %(user)s login CAS utilisateurice Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-01-30 20:48+0100
Last-Translator: Tom Hubrecht <tom.hubrecht@ens.fr>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.1
 Old CAS account Old CAS account %(cas_login)s (year %(entrance_year)s) linked to %(user)s Old clipper username Old CAS accounts No user exists with this username, this entrance year and/or this password. Please check your entry. Attention, all fields are case sensitive! Clipper CAS account CAS accounts Password login Alumni login The %(site_name)s team Connection method Password Password changed successfully! Forgot your password? New password For your information, your username is the following: %(username)s Entrance year Someone (probably you) has asked to reset the password associated with this address on %(site_name)s. Password reset If it is indeed you, you can go to the following address to choose a new one :  A password reset email has just been send to the inidcated address, provided an account with this email exists) If your graduation is approaching, create a password for your account to benefit from the alumni connection method: <a href="%(url_reset)s">create password</a>. Alumni old CAS username year of CAS account creation CAS account %(cas_login)s (year %(entrance_year)s) linked to %(user)s CAS username user 